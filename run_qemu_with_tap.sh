#qemu tap network
ip tuntap add dev tap0 mode tap
ip link set tap0 up
ip route add 192.168.1.101 dev tap0
sysctl net.ipv4.ip_forward=1 net.ipv4.conf.tap0.proxy_arp=1 net.ipv4.conf.wlp60s0.proxy_arp=1
ufw disable
sudo qemu-system-x86_64 -m 4096 -enable-kvm -net nic,netdev=xxx -netdev tap,id=xxx,ifname=tap0,script=no,downscript=no fbd.iso

# on guest
# set ip
# ifconfig eth0 192.168.1.101
# add gw
# route add default gw 192.168.1.5
# add dns
# /etc/resolv.conf -> dns

sysctl net.ipv4.ip_forward=0 net.ipv4.conf.tap0.proxy_arp=0 net.ipv4.conf.wlp60s0.proxy_arp=0
ip link set tap0 down
ip link delete tap0
ufw enable
